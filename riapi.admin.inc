<?php
/**
 * @file
 * Provide form setting, validate, and submit function for administration page.
 */

/**
 * Function riapi_admin_settings().
 */
function riapi_admin_settings() {
  // Check image repository directory.
  $image_repository = variable_get('riapi_repository', NULL);
  if (empty($image_repository)) {
    drupal_set_message(t('Restful Image unable to create image directory, please reinstall the module.'), 'error');
    drupal_goto('admin/reports/status');
  }
  $form = array();
  // Link to add image location.
  $link_add_image_location = l(t('add image location'), 'admin/config/services/riapi/add');
  $form['add_location'] = array(
    '#markup' => $link_add_image_location,
  );
  // Example riapi urls.
  // Description fieldset.
  $form['example'] = array(
    '#type' => 'fieldset',
    '#title' => t('Restful image urls'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  global $base_url;
  $examples[] = $base_url . '/riapi/[Image Path]/[Image Filename].[Image Extension]';
  $examples[] = $base_url . '/riapi/[Short Image Path]/[Image Filename].[Image Extension]';
  $examples[] = $base_url . '/[Short Image Path]/[Image Filename].[Image Extension]';
  $examples[] = $base_url . '/sites/default/files/riapi/[Short Image Path]/[Image Filename].[Image Extension]';
  $form['example']['urls'] = array(
    '#markup' => '<ul><li>' . implode('</li><li>', $examples) . '</li></ul>',
  );
  // List of image location.
  $locations = riapi_get_locations();
  // Check if image location is already defined.
  if (!empty($locations)) {
    // Define table header.
    $header = array(
      t('Image Path'), t('Type'), t('Short Image Path'), t('Action'),
    );
    // Define table rows.
    foreach ($locations as $location) {
      $rows[] = array(
        $location->location,
        $location->type,
        $location->shortcut,
        l(t('delete'), 'admin/config/services/riapi/delete/' . $location->iid),
      );
    }
    // Show as table markup.
    $form['locations'] = array(
      '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
    );
  }
  else {
    drupal_set_message(filter_xss(t('You need to !add first.', array('!add' => $link_add_image_location))), 'warning');
  }
  // Query command.
  $form['commands'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available components'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Add each component to the form:
  $form['commands'] = array('#tree' => TRUE);
  $commands = riapi_commands(TRUE);
  $disable_commands = variable_get('riapi_disabled_commands', array());
  foreach ($commands as $key => $command) {
    // Define default value for checkbox.
    $default_value = (array_search($key, $disable_commands) === FALSE) ? 1 : 0;
    // Check for q parameter.
    if (isset($command['shorthand']) && $command['shorthand'] == 'q') {
      $default_value = 0;
      drupal_set_message(t('Command can not use shorthand "q" because is used by Drupal system.'), 'error');
    }
    $form['commands'][$key] = array(
      '#title' => $command['label'],
      '#title_display' => 'invisible',
      '#description' => array(
        'command' => $key,
        'shorthand' => (isset($command['shorthand'])) ? $command['shorthand'] : '-',
        'description' => $command['description'],
        'default' => (isset($command['default']) ? $command['default'] : FALSE),
      ),
      '#type' => 'checkbox',
      '#return_value' => 1,
      '#default_value' => $default_value,
    );
  }
  // Supported image extensions.
  $form['extension'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available Image Extensions'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Choose allowed image extension to use this service.'),
  );
  // Get image toolkit.
  $image_toolkit = variable_get('image_toolkit', 'gd');
  // Define supported image extensions.
  $supported_image_extensions = array();
  // Check for GD image extension.
  if ($image_toolkit == 'gd' && extension_loaded('gd')) {
    // Get GD info.
    $info = gd_info();
    // Check for gif create support.
    if (isset($info['GIF Create Support']) && $info['GIF Create Support'] === TRUE) {
      $supported_image_extensions[] = 'gif';
    }
    // Check for jpeg support.
    if (isset($info['JPEG Support']) && $info['JPEG Support'] === TRUE) {
      $supported_image_extensions[] = 'jpeg';
      $supported_image_extensions[] = 'jpg';
    }
    // Check for png support.
    if (isset($info['PNG Support']) && $info['PNG Support'] === TRUE) {
      $supported_image_extensions[] = 'png';
    }
    // Check for wbmp support.
    if (isset($info['WBMP Support']) && $info['WBMP Support'] === TRUE) {
      $supported_image_extensions[] = 'wbmp';
    }
    // Check for xbm support.
    if (isset($info['XBM Support']) && $info['XBM Support'] === TRUE) {
      $supported_image_extensions[] = 'xbm';
    }
  }
  // Check for ImageMagick support.
  if ($image_toolkit == 'imagemagick' && extension_loaded('imagick')) {
    // Get Imagick queryFormats.
    $info = Imagick::queryFormats();
    $supported_image_extensions = array_map('strtolower', $info);
  }
  // Define checkboxes for supported image extensions.
  $form['extension']['riapi_image_extensions'] = array(
    '#type' => 'select',
    '#options' => drupal_map_assoc($supported_image_extensions),
    '#title' => t('Supported Image Extensions'),
    '#title_display' => 'invisible',
    '#multiple' => TRUE,
    '#default_value' => riapi_get_extensions(),
    '#description' => t('Allow to select multiple image extensions.'),
  );
  // Query options.
  $form['query_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Query Options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Get separator symbol from php ini.
  $separator = ini_get('arg_separator.input');
  $form['query_options']['query_separator'] = array(
    '#type' => 'item',
    '#title' => t('Query Separator'),
    '#markup' => $separator,
    '#description' => t('Change this value at php.ini.'),
  );
  $description = t('Choose between standard or shorthand command query syntax.');
  $description .= '<br />';
  $description .= t('standard: ?width=200!separatorheight=100#', array('!separator' => $separator));
  $description .= '<br />';
  $description .= t('shorthand: ?w=200!separatorh=100#', array('!separator' => $separator));
  $description .= '<br />';
  $description .= t('both: ?width=200!separatorh=100#', array('!separator' => $separator));
  $form['query_options']['riapi_query_syntax'] = array(
    '#type' => 'select',
    '#title' => t('Query Syntax'),
    '#options' => array(
      0 => t('Use standard command.'),
      1 => t('Use shorthand command.'),
      2 => t('Accept both command.'),
    ),
    '#default_value' => variable_get('riapi_query_syntax', 2),
    '#description' => $description,
  );

  $form['default'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Image'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Show default image.
  $default_image = variable_get('riapi_default_image', 'public://riapi/default.jpg');
  $logo_info = image_get_info($default_image);
  $form['default']['image'] = array(
    '#type' => 'item',
    '#title' => t('Default image'),
    '#markup' => '<img src="' . file_create_url($default_image) . '" title="' . t('Default Image') . '" alt="' . t('Default Image') . '" />',
    '#description' => ($logo_info !== FALSE) ? t('Image info:') . $logo_info['width'] . 'x' . $logo_info['height'] : '',
  );
  // Upload default image.
  $form['default']['upload'] = array(
    '#title' => t('Upload default image'),
    '#type' => 'file',
    '#description' => t('The uploaded image will be displayed if requested image is not found.'),
  );

  // Development field.
  $form['devel'] = array(
    '#type' => 'fieldset',
    '#title' => t('Development'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['devel']['riapi_init'] = array(
    '#title' => t('Use hook_init'),
    '#description' => t('Detect request at init flow not at normal flow. This option is experimental and may degrade/improve performance.'),
    '#type' => 'checkbox',
    '#return_value' => 1,
    '#default_value' => variable_get('riapi_init', 0),
  );
  // Cache image selector.
  $period = drupal_map_assoc(array(
    0, 300, 1800, 3600, 10800, 21600, 32400,
    43200, 86400, 604800, 2592000, 31536000,
  ), 'format_interval');
  $period[0] = '<' . t('none') . '>';
  $form['devel']['riapi_cache'] = array(
    '#type' => 'select',
    '#title' => t('Minimum image cache lifetime'),
    '#options' => $period,
    '#default_value' => variable_get('riapi_cache', 0),
    '#description' => t('Cached images will not be re-created until at least this much time has elapsed.'),
  );
  // Download timeout.
  $form['devel']['riapi_download_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Download Timeout'),
    '#description' => t('Define download image timeout.'),
    '#default_value' => variable_get('riapi_download_timeout', 5.0),
    '#field_suffix' => t('s'),
    '#size' => 6,
    '#maxlength' => 5,
  );

  $enable_image_module = variable_get('riapi_image_module', 0);
  $enabled_image_module = module_exists('image');
  // Link to image module.
  $form['image_module'] = array(
    '#type' => 'fieldset',
    '#title' => t('Image Module'),
    '#description' => ($enabled_image_module) ? l(t('Add/edit image style.'), 'admin/config/media/image-styles') : t('You need to enable image module for this integration.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Image module integration.
  if ($enable_image_module && $enabled_image_module) {
    // Set form styles as tree.
    $form['styles'] = array('#tree' => TRUE);
    // Get styles settings.
    $riapi_styles = variable_get('riapi_styles', array());
    // Load image admin file to get theme functions.
    module_load_include('inc', 'image', 'image.admin');
    // Get all image styles.
    $styles = image_styles();
    foreach ((array) $styles as $style) {
      $effects = array();
      foreach ($style['effects'] as $effect) {
        $function = (isset($effect['summary theme'])) ? 'theme_' . $effect['summary theme'] : '';
        $effects[] = (function_exists($function)) ? $effect['label'] . ' ' . $function($effect) : $effect['label'];
      }
      $form['styles'][$style['name']] = array(
        '#title' => $style['label'],
        '#title_display' => 'invisible',
        '#description' => array(
          'name' => $style['name'],
          'effect' => implode('<br />', $effects),
        ),
        '#type' => 'checkbox',
        '#return_value' => 1,
        '#default_value' => in_array($style['name'], $riapi_styles),
      );
    }
  }
  $form['image_module']['riapi_image_module'] = array(
    '#type' => 'checkbox',
    '#title' => t('Integrate to image module.'),
    '#description' => t('Image style will be applied when there is request. Location public://riapi/[shortpath]/[directory]/imagefilename.ext.'),
    '#return_value' => 1,
    '#default_value' => $enable_image_module,
    '#disabled' => ($enabled_image_module) ? FALSE : TRUE,
  );

  // Set theme for this form.
  $form['#theme'] = 'riapi_admin_settings';
  // Add submit callback.
  $form['#submit'][] = 'riapi_admin_settings_submit';
  // Return system setting form: auto set variables.
  return system_settings_form($form);
}

/**
 * Function riapi_admin_settings_submit().
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function riapi_admin_settings_submit(array $form, array &$form_state) {
  // Process disable/enable commands: $form_state['values']['commands'].
  $disable_commands = array();
  foreach ($form_state['values']['commands'] as $command => $value) {
    if (empty($value)) {
      $disable_commands[] = $command;
    }
  }
  variable_set('riapi_disabled_commands', $disable_commands);

  // Define file upload valid extension.
  $valid_extension = riapi_get_extensions();
  $valid_extension = (!empty($valid_extension)) ? array(implode(' ', $valid_extension)) : array('png gif jpg jpeg');
  // Check file upload.
  $file = file_save_upload('upload', array(
    'file_validate_is_image' => array(),
    'file_validate_extensions' => $valid_extension,
  ), variable_get('riapi_repository', 'public://riapi'), FILE_EXISTS_REPLACE);
  // Check uploaded default image.
  if (is_object($file) && isset($file->filename) && preg_match('#\.[a-z]{3,4}$#i', $file->filename, $match)) {
    $file->status = 1;
    $default_image = variable_get('riapi_default_image', 'public://riapi/default.jpg');
    $default_image = preg_replace('#\.[a-z]{3,4}$#i', $match[0], $default_image);
    // Move it to replace old default image.
    file_move($file, $default_image, FILE_EXISTS_REPLACE);
    // Set updated default image.
    variable_set('riapi_default_image', $default_image);
  }

  // Check image download timeout value: must use float value.
  $form_state['values']['riapi_download_timeout'] = floatval($form_state['values']['riapi_download_timeout']);
  // Download timeout must have decimal value.
  $form_state['values']['riapi_download_timeout'] = number_format($form_state['values']['riapi_download_timeout'], 1);

  // Check image module.
  if (!module_exists('image')) {
    // If not disable, always to disable image module integration.
    $form_state['values']['riapi_image_module'] = 0;
    // Disable riapi image styles variables.
    variable_set('riapi_image_module', 0);
    variable_set('riapi_styles', array());
  }
  else {
    if (isset($form_state['values']['styles'])) {
      $styles = array_filter($form_state['values']['styles']);
      $styles = array_keys($styles);
      variable_set('riapi_styles', $styles);
      // Get all riapi image path.
      $locations = riapi_get_locations();
      foreach ((array) $locations as $location) {
        // Create directory for style if not already created.
        foreach ((array) $styles as $style) {
          $directory = variable_get('riapi_repository', 'public://riapi') . '/' . $location->shortcut . '/' . $style;
          file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
        }
      }
    }
  }

  // Remove internal Form API values.
  unset($form_state['values']['commands'], $form_state['values']['upload'], $form_state['values']['styles']);
}

/**
 * Function riapi_image_location_form().
 */
function riapi_image_location_form() {
  // Add js to give alert about image download permissions.
  $js = '
    (function ($) {
      Drupal.behaviors.imageLocation = {
        attach: function (context, settings) {
          $("form", context).submit(function () {
            if ($("#edit-path").val() === "") {
              alert("' . t('Image path can not empty.') . '");
              return false;
            }
            else {
              alert("' . t('Make sure you have necessary permission to access the image at:') . ' " + $("#edit-path").val());
            }
          });
        }
      };
    })(jQuery);
  ';
  drupal_add_js($js, 'inline');
  // Define form array.
  $form = array();
  // Define page description.
  $description = t('Image location can be local or outside server (only support http protocol).');
  $description .= '<br />';
  $description .= t('Example local image path: /var/www/image/test.jpg');
  $description .= '<br />';
  $description .= t('Example outside server image path: http://test.com/image/test.jpg.');
  $form['description'] = array(
    '#markup' => $description,
  );
  // Define image path input: textfield.
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Image Path'),
    '#default_value' => '',
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#description' => t('Enter a image full path or image full url.'),
  );
  // Define image shortcut path input: textfield.
  $form['shortcut'] = array(
    '#type' => 'textfield',
    '#title' => t('Short Image Path'),
    '#default_value' => '',
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#description' => t('Provide short image path to create shorter url.'),
  );
  // Define add button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add Image Location'),
  );

  return $form;
}

/**
 * Function riapi_image_location_form_validate().
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function riapi_image_location_form_validate(array $form, array &$form_state) {
  // Check path must contain image.
  if (!preg_match('#\/[^\?\/]+\.[a-z]{3,4}$#', $form_state['values']['path'], $match)) {
    form_set_error('path', t('Image path must contain one image to verify the path.'));
  }
  else {
    // Check whether path is outside url or local directory.
    if (preg_match('#^http://#', $form_state['values']['path'])) {
      // Path is outside url: do http request to image url.
      $result = drupal_http_request($form_state['values']['path']);
      // Check for http 200.
      if (!(isset($result->code) && $result->code == 200)) {
        // Log this condition.
        watchdog('riapi', 'Unable to get image url to verify image location. Debug: <pre>@result</pre>', array(
          '@result' => var_export($result, TRUE),
        ), WATCHDOG_ERROR, l(t('Riapi: verify image url'), request_path()));
        form_set_error('path', t('Image url is unreachable.'));
      }
    }
    else {
      // Path is local directory: get correct directory.
      $directory = realpath(str_replace($match[0], '', $form_state['values']['path']));
      // Check whether image path is readable directory.
      if (!(is_dir($directory) && is_readable($directory))) {
        form_set_error('path', t('Image full path is not readable directory.'));
      }
    }
  }

  // Check shortcut pattern: a-z0-9.
  if (!preg_match('#^[a-z0-9]+$#i', $form_state['values']['shortcut'])) {
    form_set_error('shortcut', t('Shortcut can only contain alphabet (a-z0-9) characters.'));
  }
  elseif (preg_match('#riapi#i', $form_state['values']['shortcut'])) {
    form_set_error('shortcut', t('Shortcut can not contain reserved word "riapi".'));
  }
  else {
    // Strip path from any protocol.
    $path = preg_replace('#^(http\:\/)?\/#i', '', $form_state['values']['path']);
    // Strip path from any image files.
    $path = preg_replace('#\/[^\?\/]+\.[a-z]{3,4}$#', '', $path);
    // Check whether image path and short path already registered.
    $found = db_select('riapi_location', 'r')
      ->fields('r')
      ->condition('shortcut', $form_state['values']['shortcut'])
      ->condition('location', $path)
      ->countQuery()
      ->execute()
      ->fetchField();
    if ($found > 0) {
      form_set_error('shortcut', t('Image path and shortcut already defined. Please define another shortcut.'));
    }
  }
}

/**
 * Function riapi_image_location_form_submit().
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function riapi_image_location_form_submit(array $form, array &$form_state) {
  // Get image location type.
  $type = (preg_match('#^http#', $form_state['values']['path'])) ? 'http' : 'local';
  // Strip path from any protocol.
  $form_state['values']['path'] = preg_replace('#^(http\:\/)?\/#i', '', $form_state['values']['path']);
  // Strip path from any image files.
  $form_state['values']['path'] = preg_replace('#\/[^\?\/]+\.[a-z]{3,4}$#', '', $form_state['values']['path']);
  // Save image location to database.
  db_insert('riapi_location')
    ->fields(array(
      'location' => $form_state['values']['path'],
      'type' => $type,
      'shortcut' => $form_state['values']['shortcut'],
    ))
    ->execute();
  // Cache reset: riapi_location from cache_riapi bin.
  cache_clear_all('riapi_location', 'cache_riapi');
  // Check or create shortcut directory.
  $directory = variable_get('riapi_repository', 'public://riapi') . '/' . $form_state['values']['shortcut'];
  file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
  // Set success message.
  drupal_set_message(t('Success add new image location.'));
  // Redirect to settings page.
  $form_state['redirect'] = 'admin/config/services/riapi';
}

/**
 * Theme the output of the riapi_admin_settings() form.
 */
function theme_riapi_admin_settings($variables) {
  $form = $variables['form'];

  // Commands: Format the components into a table.
  foreach (element_children($form['commands']) as $key) {
    $row = array();
    $row[] = $form['commands'][$key]['#title'];
    $row[] = $key;
    $row[] = $form['commands'][$key]['#description']['shorthand'];
    $row[] = ($form['commands'][$key]['#description']['default'] == FALSE) ? '-' : $form['commands'][$key]['#description']['default'];
    $row[] = $form['commands'][$key]['#description']['description'];
    $form['commands'][$key]['#description'] = NULL;
    $row[] = array(
      'data' => drupal_render($form['commands'][$key]),
      'align' => 'center',
    );
    $rows[] = array(
      'data' => $row,
      'class' => ($form['commands'][$key]['#checked']) ? array() : array('warning'),
    );
  }
  // Create the table inside the form.
  $form['commands']['table'] = array(
    '#theme' => 'table',
    '#header' => array(
      t('Name'), t('Command'), t('Shorthand'), t('Default'), t('Description'), array(
        'data' => t('Enabled'),
        'class' => array('checkbox'),
      ),
    ),
    '#rows' => $rows,
  );

  // Image Module: format the components into a table.
  if (isset($form['styles'])) {
    $rows = array();
    foreach (element_children($form['styles']) as $key) {
      $row = array();
      $row[] = $form['styles'][$key]['#title'];
      $row[] = $form['styles'][$key]['#description']['name'];
      $row[] = $form['styles'][$key]['#description']['effect'];
      $form['styles'][$key]['#description'] = NULL;
      $row[] = array(
        'data' => drupal_render($form['styles'][$key]),
        'align' => 'center',
      );
      $rows[] = $row;
    }
    // Create the table inside the form.
    $form['image_module']['table'] = array(
      '#theme' => 'table',
      '#header' => array(
        t('Label'), t('Directory/Image Path'), t('Effects'), t('Enabled'),
      ),
      '#rows' => $rows,
    );
  }

  return drupal_render_children($form);
}

/**
 * Function riapi_delete_image_directory().
 *
 * @param int $iid
 *   Image location ID.
 */
function riapi_delete_image_directory($iid = 0) {
  $iid = intval($iid);
  if ($iid > 0) {
    // Delete image location from database.
    db_delete('riapi_location')
      ->condition('iid', $iid)
      ->execute();
    // Reset riapi_location cache.
    cache_clear_all('riapi_location', 'cache_riapi');
    drupal_set_message(t('Success delete image location from database only.'));
  }
  else {
    drupal_set_message(t('Unable to delete image location.'), 'error');
  }
  // Redirect to main administration page.
  drupal_goto('admin/config/services/riapi');
}
