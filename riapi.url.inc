<?php
/**
 * @file
 * Provide function callback for riapi url.
 */

/**
 * Function riapi_url().
 *
 * @param string $shortcut
 *   Image shortcut path.
 */
function riapi_url($shortcut = '') {
  // Prerequisite #0: Check cache.
  // Create hash cache.
  $cache_hash = 'riapi-' . drupal_hash_base64(request_uri());
  // Get data from cache: if available at cache_riapi not in cache_page.
  $cache = cache_get($cache_hash, 'cache_riapi');
  // Check whether cache is available.
  if (empty($cache)) {
    // Prerequisite #1: valid image request.
    // Get current path: riapi/[imagepath]/[imagefile].[imageext]
    // But remove "riapi/" at the beginning, if available.
    $path = preg_replace('#^riapi/#', '', current_path());
    // Get allowed image extensions.
    $image_extensions = riapi_get_extensions();
    // Check and get image filename.
    if (!preg_match('#\/[^\/]+\.(' . implode('|', $image_extensions) . ')$#', $path, $image)) {
      // No image request/unsupported image extension:
      // Exit from this function, let drupal/webserver handle 404 page.
      return;
    }
    // Prerequisite #2: valid path request.
    // Get image location target: by shortcut first from the function input.
    if (!$target_location = riapi_get_locations($shortcut)) {
      // If shortcut is not available: use current target location.
      $target_location = str_replace($image[0], '', $path);
      // Get all image locations.
      $target_location = riapi_get_locations($target_location);
      // Redefine shortcut: from the first target location array.
      $shortcut = (isset($target_location[0]->shortcut)) ? $target_location[0]->shortcut : $shortcut;
    }
    // Check whether target location is registered.
    if (empty($target_location)) {
      // No valid image target location request: do 404.
      drupal_not_found();
      drupal_exit();
    }

    // Define default repository.
    $default_repository = variable_get('riapi_repository', 'public://riapi') . '/';
    // Define default image.
    $default_image = variable_get('riapi_default_image', 'public://riapi/default.jpg');

    // Process #1: Get Original Image.
    // Define target image: based on requested image filename.
    $target_image = $default_repository . $shortcut . $image[0];
    // Check image local availability: for both local and http.
    // @todo: monitor this section, performance may degrade.
    if (!file_exists($target_image)) {
      // Check for each defined target locations.
      foreach ((array) $target_location as $location) {
        // Define target image: based on requested image filename.
        $target_image = $default_repository . $location->shortcut . $image[0];
        // Get original image.
        switch ($location->type) {
          case 'local':
            // Copy for distant directory into drupal files directory.
            // If failed, will assign FALSE to $target_image.
            $target_image = file_unmanaged_copy('/' . $location->location . $image[0], $target_image, FILE_EXISTS_REPLACE);
            break;

          case 'http':
            // Download image into drupal files directory.
            // If failed, will assign FALSE to $target_image.
            $target_image = riapi_download_image('http://' . $location->location . $image[0], $target_image);
            break;
        }
        // Check if target already acquired!
        if (!empty($target_image)) {
          // Breaking the array loop.
          break;
        }
      }
      // Get default image: as a last resort.
      // How to check: $target_image with FALSE.
      $target_image = (empty($target_image)) ? $default_image : $target_image;
    }

    // Process #2: process effect.
    // Define effect container.
    $effects = array();
    // Get query syntax.
    $syntax = variable_get('riapi_query_syntax', 2);
    // Get riapi commands.
    $commands = riapi_commands();
    // Check for each commands defined.
    foreach ((array) $commands as $command => $command_data) {
      // Define added input command as NULL.
      $input_command = NULL;
      // Check default value.
      if (isset($command_data['default']) && !empty($command_data['default'])) {
        $input_command = $command_data['default'];
      }
      // Check short syntax first.
      if ($syntax >= 1 && isset($_GET[$command_data['shorthand']])) {
        $input_command = $_GET[$command_data['shorthand']];
      }
      // Check normal syntax (overwrite short syntax).
      if ($syntax != 1 && isset($_GET[$command])) {
        $input_command = $_GET[$command];
      }
      // Finally, check whether system received input command.
      if (!is_null($input_command)) {
        // Check whether command need to include another command file.
        if (isset($command_data['file'])) {
          $pathinfo = pathinfo($command_data['file']);
          $basename = basename($pathinfo['basename'], '.' . $pathinfo['extension']);
          $path = (!empty($pathinfo['dirname']) ? $pathinfo['dirname'] . '/' : '') . $basename;
          module_load_include($pathinfo['extension'], $command_data['module'], $path);
        }
        // Check and run validate function.
        if (isset($command_data['input_callback'])
          && function_exists($command_data['input_callback'])
          && function_exists($command_data['image_callback'])
        ) {
          // Add it in effect container.
          $effects[$command_data['image_callback']][$command] = $command_data['input_callback']($input_command);
        }
      }
    }

    // Check if need effect processing.
    if (!empty($effects)) {
      // Hash the serialize effect.
      $hash = sha1(serialize($effects));
      // Store original target image.
      $original_target_image = $target_image;
      // Modify target image: add hash.
      $target_image = preg_replace('#\.[a-z]{3,4}$#', '-' . $hash . '$0', $target_image);
      // Check image local availability: for both local and http.
      // @todo: monitor this section, performance may degrade.
      if (!file_exists($target_image)) {
        // Load the original image.
        $original_image = image_load($original_target_image);
        // Check for each effect.
        foreach ($effects as $effect => $effect_data) {
          // Call effect functions.
          call_user_func_array($effect, array($original_image) + $effect_data);
        }
        // Then save it: If unable to save, rollback to default image.
        if (!image_save($original_image, $target_image)) {
          // Target image is current process image.
          $target_image = $original_target_image;
        }
      }
    }

    // Process #3: process image styles.
    // Get flag image module integration.
    $enable_image_module = variable_get('riapi_image_module', 0);
    // Get all enabled image styles.
    $riapi_styles = variable_get('riapi_styles', array());
    // Only checking when enable.
    if ($enable_image_module && !empty($riapi_styles)) {
      // Get 3rd arguments.
      $style = arg(2);
      // Check if style enabled.
      if (in_array($style, $riapi_styles)) {
        // Store original target image.
        $original_target_image = $target_image;
        // Define image destination.
        $target_image = str_replace($image[0], '/' . $style . $image[0], $target_image);
        // Check image local availability: for both local and http.
        // @todo: monitor this section, performance may degrade.
        if (!file_exists($target_image)) {
          // Load image style.
          $image_style = image_style_load($style);
          if (!empty($image_style)) {
            // Create image derivative.
            if (!image_style_create_derivative($image_style, $original_target_image, $target_image)) {
              // Change target image.
              $target_image = $original_target_image;
            }
          }
        }
      }
    }

    // Process #4: Get image info: to build image header for download.
    $image_cache = variable_get('riapi_cache', 0);
    $image_info = image_get_info($target_image);
    $image_headers = array(
      'Cache-Control' => 'public, max-age=' . $image_cache,
      'Connection' => 'close',
      'Content-Type' => $image_info['mime_type'],
      'Content-Length' => $image_info['file_size'],
      'ETag' => '"' . REQUEST_TIME . '"',
      'Expires' => gmdate('D, d M Y H:i:s', time() + $image_cache) . ' GMT',
      'Last-Modified' => gmdate('D, d M Y H:i:s', REQUEST_TIME) . ' GMT',
      'status' => '200 OK',
    );

    // Process #5: cache the result.
    // Check if target image is default image, do not cache yet.
    if ($default_image != $target_image) {
      cache_set($cache_hash, array(
        'uri' => $target_image,
        'headers' => $image_headers,
      ), 'cache_riapi', $image_cache);
    }
  }
  else {
    // Define variable needed to transfer the image.
    $target_image = $cache->data['uri'];
    $image_headers = $cache->data['headers'];
    // Set X-Drupal-Cache as HIT.
    header('X-Drupal-Cache: HIT');
  }

  // Final: deliver the image to user (last step).
  // Transfer image file: also end of drupal process.
  file_transfer($target_image, $image_headers);
}
