<?php
/**
 * @file
 * Provide function as riapi image callback related to quality parameter.
 */

/**
 * Function riapi_image_quality().
 *
 * @param object $image
 *   Drupal Image Object, an image object returned by image_load().
 * @param int $quality
 *   Quality parameter.
 *
 * @return bool
 *   Flag image processing is success or not.
 */
function riapi_image_quality($image, $quality = 90) {
  // Define return flag.
  $return = FALSE;
  // Check image extension: only for jpg and jpeg.
  if (isset($image->info['extension']) && !empty($image->info['extension'])) {
    switch ($image->info['extension']) {
      case 'jpeg':
      case 'jpg':
        // Quality between 0 - 100.
        $quality = ($quality >= 0 && $quality <= 100) ? $quality : 90;
        // Only modify Drupal image_jpeg_quality variable.
        global $conf;
        $conf['image_jpeg_quality'] = $quality;
        $return = TRUE;
        break;
    }
  }
  return $return;
}
