Description
-----------
This module try to build Drupal as Restful Image service.
Original images can be located at different web server or local server but 
different directories.
Module extensively uses file and image functions provided by Drupal.

Process:
Module will download the image, process it based on request parameters, and 
output it with correct HTTP headers. This process is on-the-fly and cacheable 
to improve response time for next request.

Example:
Original Image Location
http://www.example.com/product/images/image.png
Drupal Restful Image location
$base_url/riapi/www.example.com/product/images/image.png
Defacto Image location
$base_url/sites/default/files/riapi/image.png
Alternate short location (www.example.com/product/images shorten as static)
$base_url/riapi/static/image.png
Process Image (scale down based on width)
$base_url/riapi/static/image.png?w=200
Image Derivative (integration with thumbnail image style from image module)
$base_url/riapi/static/thumbnail/image.png

Scenarios:
1. Image upload process already handled by other server which has no further 
processing capabilities (scaling, optimizing).
2. Unable using image service such as AWS S3, because the service is too 
expensive.

Current problems:
1. quality parameter (q) can not be used because q is used by Drupal.
2. to improve response time, server configuration is needed. 
(tested using nginx and apache)

Resources:
http://riapi.org

Requirements
------------
Drupal 7.x

Installation
------------
1. Copy the entire riapi directory the Drupal sites/all/modules directory.
2. Login as an administrator. Enable the module in the "Administration" -> 
"Modules"
3. Edit the riapi configuration file under "Administration" -> "Configuration" 
-> "Web Services" -> "Restful Image"
4. Add image location under "Administration" -> "Configuration" ->
"Web Services" -> "Restful Image" -> "Add Image Location"
